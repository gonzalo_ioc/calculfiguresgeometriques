/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author gon
 */
public class Rectangle  implements FiguraGeometrica{
    
        private  double base;
        private double alcada;
    
    public Rectangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la base  del rectangle: ");
        this.base = scanner.nextDouble();
                System.out.println("Introduïu l`altura  del rectangle: ");
        this.alcada = scanner.nextDouble();
    }
    
    @Override
    public double calcularArea() {
        return base * alcada;
    }
    
    @Override
    public double calcularPerimetre() {
        return 2+(base + alcada);
    }
}
